const fs = require("fs");

const crabPositions = fs
  .readFileSync("input-part1.txt")
  .toString()
  .trim()
  .split(",")
  .map((x) => parseInt(x))
  .sort();

const min = Math.min(
  ...crabPositions.map((_, i) =>
    crabPositions
        .map((x) => Math.abs(x - i))
        .reduce((a, b) => a + b)
  )
);
console.log(min);
